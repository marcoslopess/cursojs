//operador ' ... '  rest(juntar)/spread(espalhar) 
//operador spread

//objeto
const funciionario = { nome: 'Maria', salario: 2500.89 }
const clone = { ativo: true, ...funciionario }
console.log(clone);

//array
const grupoA = ['paulo', 'lopes', 'da']
const grupoFinal = ['marcos', ...grupoA, 'costa']
console.log(grupoFinal);
