const pilotos = ['Vettel', 'Alonso', 'Sena', 'Massa']

//remove o ultimo elemsnto do array
pilotos.pop()

//adiciona um elemento no final da lista
pilotos.push('Verstappen')
console.log(pilotos)

//remove o primeiro
pilotos.shift()
console.log(pilotos)


//adiciona um elemento no inicio da lista
pilotos.unshift('hamilton')
console.log(pilotos)

//adiciona e remove elementos

//adicionar
pilotos.splice(2, 0, 'Bottas', 'Massa')
console.log(pilotos)


//remover
pilotos.splice(3, 1)
console.log(pilotos)

//cria um novo array
const algunsPilotos1 = pilotos.slice(2)
console.log(algunsPilotos1)

//cria até o indice 4 mas não adiciona o indice 4
const algunsPilotos2 = pilotos.slice(1, 4)
console.log(algunsPilotos2)

