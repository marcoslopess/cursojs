import React from 'react'
import ReactDom from 'react-dom'
import Header from './componentes/header/index'
import Routes from './routes'
//import Table from './componentes/table/index'



ReactDom.render(
    <div>
        <Header></Header>
        <Routes></Routes>
    </div>
    , document.getElementById('root'))


     /* 
     import Pai from './componentes/Pai'
import Filho from './componentes/Filho'
     
     <div>
        <Pai nome="Marcos" sobrenome="Lopes">
            <Filho nome="Kaique" />
            <Filho nome="Patricia" />
            <Filho nome="Millena" />
        </Pai>
</div>*/