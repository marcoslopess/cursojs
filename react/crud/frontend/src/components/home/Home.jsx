import React from 'react'
import Main from '../templates/Main'

export default props =>
    <Main icon="home" title="Inicio"
        subtitle="Rotina de logística">
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col text-center mb-4">
                <div class="card card text-white bg-warning h-100">
                    <div class="card-body">
                        <h9 class="card-title">Novos Pedidos Por Rota</h9>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">16</h4>
                    </div>
                </div>
            </div>
            <div class="col text-center mb-4">
                <div class="card card text-white bg-primary h-100">
                    <div class="card-body">
                        <h9 class="card-title">Cargas Montadas e Confirmadas</h9>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">93</h4>
                    </div>
                </div>
            </div>
            <div class="col text-center mb-4">
                <div class="card card text-white bg-success h-100">
                    <div class="card-body">
                        <h9 class="card-title">Cargas Entregues</h9>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">63</h4>
                    </div>
                </div>
            </div>
            <div class="col text-center mb-4">
                <div class="card card text-white bg-danger h-100">
                    <div class="card-body">
                        <h9 class="card-title">Retorno - Pendência</h9>
                        <br/>
                        <h9 class="card-title">Aberto</h9>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">9</h4>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col text-center mb-4">
                <div class="card card text-white bg-warning h-100">
                    <div class="card-body">
                        <h9 class="card-title">Novos Pedidos Global</h9>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">16</h4>
                    </div>
                </div>
            </div>
            <div class="col text-center mb-4">
                <div class="card card text-white bg-primary h-100">
                    <div class="card-body">
                        <h9 class="card-title">Total de Pedidos em Cargas</h9>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">337</h4>
                    </div>
                </div>
            </div>
            <div class="col text-center mb-4">
                <div class="card card text-white bg-success h-100">
                    <div class="card-body">
                        <h9 class="card-title">Total de Pedidos Entregues</h9>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">233</h4>
                    </div>
                </div>
            </div>
            <div class="col text-center mb-4">
                <div class="card card text-white bg-danger h-100">
                    <div class="card-body">
                        <h9 class="card-title">Retorno - Pendência</h9>
                        <br/>
                        <h9 class="card-title">Fechado</h9>
                    </div>
                    <div class="card-footer">
                        <h4 class="card-title">X</h4>
                    </div>
                </div>
            </div>
        </div>

    </Main>