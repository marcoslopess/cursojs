const produtos = [
    { nome: 'notebook', preco: 2499, fragil: true },
    { nome: 'iphone', preco: 3000, fragil: true },
    { nome: 'copo de vidro', preco: 12.49, fragil: true },
    { nome: 'copo de plastico', preco: 18.99, fragil: false }
]

const barato = produto => produto.preco >= 10
const fragile = produto => produto.fragil

console.log(produtos.filter(barato).filter(fragile))