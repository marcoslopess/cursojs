import React, { Component } from 'react'
import api from '../../services/api'

export default class Municipio extends Component {

    state = {
        _municipio : {},
    }

    async componentDidMount() {
        const { id } = this.props.match.params;
        const response = await api.get(`/municipio/${id}`);
        this.setState({ _municipio: response.data });
       
        
    }

    render() {
        const { _municipio } = this.state
        console.log( _municipio.DESCR);
        return (
            <h1>{ _municipio.DESCR }</h1>
            
        )
    }
}