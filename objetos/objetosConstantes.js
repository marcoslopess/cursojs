//pessoa -> 123 -> {...}
const pessoa = { nome: 'João'}
pessoa.nome = 'Pedro'
console.log(pessoa)


//pessoa -> 456 -> {...}
//da erro pq esta tentando declara um novo objeto a constante
//pessoa = {nome: 'Ana'}


//congela o objeto, fazendo com que o objeto est totalmente constante
Object.freeze(pessoa)

pessoa.nome = 'Maria'
pessoa.end = 'Rua tal'
console.log(pessoa)


//cria um novo objeto e depois congela
const pessoaConstante = Object.freeze({ nome: 'Joaozin'})
console.log(pessoaConstante)