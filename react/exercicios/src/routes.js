import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Table from './componentes/table'
import Municipio from './componentes/municipio'

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={Table} />
            <Route path='/municipio/:id' component={Municipio} />
        </Switch>
    </BrowserRouter>
)

export default Routes;