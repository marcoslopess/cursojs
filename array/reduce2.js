const alunos = [
    {nome: 'joao', nota: 7.3, bolsista: true },
    {nome: 'maria', nota: 9.2, bolsista: false },
    {nome: 'pedro', nota: 9.8, bolsista: true },
    {nome: 'ana', nota: 8.7, bolsista: false }
]

const todosBolsistas = (resultado, bolsista) => resultado && bolsista
console.log(alunos.map(a=> a.bolsista).reduce(todosBolsistas))


const algumBolsista = (resultado, bolsista) => resultado || bolsista
console.log(alunos.map(a=> a.bolsista).reduce(algumBolsista))