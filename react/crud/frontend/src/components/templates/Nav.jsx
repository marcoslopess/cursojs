import './Nav.css'
import NavItem from './NavItem'
import React from 'react'

export default props =>
<aside className="menu-area">
    <nav className="menu">
    <NavItem url="" icon="home" title="Inicio"/>
    <NavItem url="users" icon="users" title="Usuarios"/>
    </nav>
</aside>