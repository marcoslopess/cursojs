const porta = 3003


const express = require('express')
var cors = require('cors')
const app = express()
const bodyParser = require('body-parser')
const mysql = require('mysql')
//const bancoDeDados = require('./bancoDeDados')


app.use(cors())
app.use(bodyParser.urlencoded({ extended: true}))
app.use(bodyParser.json())


const router = express.Router();
router.get('/', (req, res) => res.json({ message: 'Funcionando!' }));
app.use('/', router);

// app.get('/produtos', (req, res, next) => {
//     console.log('Midedleware 1...')
//     next()
// })


function execSQLQuery(sqlQry, res){
    const connection = mysql.createConnection({
      host     : 'localhost',
      port     : 3306,
      user     : 'root',
      password : 'root',
      database : 'laravel'
    });
  
    connection.query(sqlQry, function(error, results, fields){
        if(error) 
          res.json(error);
        else
          res.json(results);
        connection.end();
    });
  }


  router.get('/municipios', (req, res) =>{
    execSQLQuery('SELECT * FROM municipio', res);
})



router.get('/municipio/:id', (req, res) =>{
    let filter = '';
    if(req.params.id) filter = ' WHERE ID =' + parseInt(req.params.id);
    execSQLQuery('SELECT * FROM municipio' + filter, res);
})

/*

app.get('/municipios', (req, res, next) => {
    res.send(bancoDeDados.getProdutos())
})

app.get('/municipios/:id', (req, res, next) => {
    res.send(bancoDeDados.getProduto(req.params.id))
})

app.post('/produtos', (req, res, next) => {
    const produto = bancoDeDados.salvarProduto({
        nome: req.body.nome,
        preco: req.body.preco
    })
    res.send(produto) //JSON
})

app.put('/produtos/:id', (req, res, next) => {
    const produto = bancoDeDados.salvarProduto({
        id: req.params.id,
        nome: req.body.nome,
        preco: req.body.preco
    })
    res.send(produto) //JSON
})


app.delete('/produtos/:id', (req, res, next) => {
    const produto = bancoDeDados.excluirProduto(req.params.id)
    res.send(produto) //JSON
})*/

app.listen(porta, () => {
    console.log(`Servidor executando na porta ${porta}.`)    
})