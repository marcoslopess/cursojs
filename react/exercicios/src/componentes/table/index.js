import React, { Component } from 'react'
import api from '../../services/api'

export default class Table extends Component {

    state = {
        _municipios: [],
    }

    async componentDidMount() {
        const response = await api.get('/municipios')
        this.setState({ _municipios: response.data })

    }

    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">CIDADE</th>
                        <th scope="col">UF</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state._municipios.map(municipios => (
                        <tr>
                        <th scope="row">{municipios.ID}</th>
                        <td>{municipios.DESCR}</td>
                        <td>{municipios.SiglaUf}</td>
                    </tr>
                    ))}
                    
                </tbody>
            </table>
        )
    }
}